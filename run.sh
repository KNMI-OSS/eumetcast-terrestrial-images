#!/usr/bin/env bash
# entrypoint script for eumetcast-terrestrial
# sets up TUN device and AMT gateway
# expects TC_USER_NAME, TC_USER_KEY, TC_CHANNEL_IP, TC_CHANNEL_ID and TC_SOURCE_IP
# environment variables to be set to configure tellicast
set -euo pipefail
echo "-----------------------------------------"
echo "eumetcast-terrestrial startup script     "
echo "IMAGE: ${IMAGE_TAG}                      "
echo "GIT HASH: ${GIT_HASH}                    "
echo "-----------------------------------------"


# amt-gateway config
if [ -z "${PRIMARY_RELAY-}" ]; then
    # Main GEANT Relay
    CFG_DIR=/etc/
    RELAY_FILE=amt-relays.txt
    PRIMARY_RELAY=`cat $CFG_DIR$RELAY_FILE | grep -v '^#' | head -n 1`
fi
PRIMARY_IP=`getent ahostsv4 $PRIMARY_RELAY | grep STREAM | head -n 1 | awk '{print $1}'`

echo "Configured relay ${PRIMARY_RELAY} with IP: ${PRIMARY_IP}"

# tunnel config
TUNNEL=tun1
TUNNEL_IP=10.0.0.11
TUNNEL_SUBNET=32
TUNNEL_MTU=1500
PRIVATE_IP=$(hostname -i)

echo "tunnel config: ${TUNNEL} ${TUNNEL_IP} ${TUNNEL_MTU} ${PRIVATE_IP}"



echo "checking for tunnel"
if ip link show $TUNNEL>/dev/null 2>&1; then
  echo "$TUNNEL exists, skipping"
else
  echo "$TUNNEL needs to be created"
  /create-tunnel $TUNNEL $TUNNEL_IP/$TUNNEL_SUBNET $TUNNEL_MTU $PRIVATE_IP
fi

echo "checking for amt gateway"
if pgrep amtgwd 2>&1; then
  echo "gateway already running skipping"
else
  echo "starting gateway"
  amtgwd -a $PRIMARY_IP -c $TUNNEL &
fi
sleep 1

envsubst < /etc/config.template > /etc/tellicast-config.ini
echo "tellicast config:  address=${TC_CHANNEL_IP}:4711 name=TSL-TER-${TC_CHANNEL_ID} source_address=${TC_SOURCE_IP}"


echo "starting tellicast"
/usr/local/bin/tc-cast-client -c /etc/tellicast-config.ini