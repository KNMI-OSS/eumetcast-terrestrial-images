# EUMETCast Terrestrial images

### what is it?

[EUMETCast](https://www.eumetsat.int/eumetcast) is EUMETSAT's primary mechanism for near real-time delivery of satellite data. This repository builds container images to run [EUMETCast Terrestrial over AMT](https://confluence.ecmwf.int/display/EWCLOUDKB/How+to+use+EUMETCast+Terrestrial+on+AMT). EUMETCast Terrestrial allows receiving EUMETCast data over the regular internet, allowing flexible deployment in public cloud environments.

### run requirements


#### Getting access

For public (not European Weather Cloud) deployments ask EUMETSAT to add your public IP(s) to the whitelist (you want to use floating IP(s)).

Environment variables with credentials provided by EUMETSAT:

`TC_USER_NAME`

`TC_USER_KEY`

#### Tellicast Configuration

Channel configuration:

`TC_CHANNEL_IP`

`TC_CHANNEL_ID`

`TC_SOURCE_IP`

(Up to date as of writing, EUMETSAT maintains latest configuration [EUMETCast wiki](https://eumetsatspace.atlassian.net/wiki/spaces/DSEC/pages/697794692/EUMETCast+Terrestrial+Service))

| Channel Name | Total Bandwidth (Mbps) | Data | Channel ID | Channel IP | Source IP |
|--------------|------------------------|------|------------|------------|-----------|
| TER-1        | 240                    | EPS, MSG, Sentinel-3A/B, Sentinel-5P, Third Party data, MTG | 1 | 232.223.222.1 | 193.17.9.3 |
| TER-2        | 168                    | Sentinel-6A | 2 | 232.223.223.1 | 193.17.9.7 |
| TER-3        | 230                    | Sentinel-5P L1B, Sentinel-3A/B OLCI L1 FR, Sentinel-3A/B SLSTR L1B, FY3 HIRAS, FY4 GIIRS, GOSAT, MTG HRFI-FD (4 high-res full-disk bands) | 3 | 232.223.224.1 | 193.17.9.4 |

extra options:
- `TC_PACKET_NAKS` (default is 0; disabled. Set to 1 upon approval of EUMETSAT).
- `PRIMARY_RELAY` (default is the first non comment in `amt-relays.txt`. set to alternative relay upon EUMETSAT approval).


#### Container configuration

the container will need run time access to `/dev/net/tun` to create a TUN device.

Capabilities:
- `NET_ADMIN`: needed to create tunnel
- `SYS_TIME`: tellicast-client has the ability (Default;off) to set system time


Sysctl settings:
> from '/etc/sysctl.conf` provided by EUMETSAT.
>```ini
> # Controls the read buffer sizes (for the Tellicast client)
> net.core.rmem_max=10000000
> # Controls the write buffer sizes (for the Tellicast client)
> net.core.wmem_max=10000000
> # Disabling default rp_filter (for the Tellicast client)
> net.ipv4.conf.default.rp_filter=0
> # Disabling all rp_filter (for the Tellicast client)
> net.ipv4.conf.all.rp_filter=0
> # Enabling ip_forward (for the Tellicast client)
> net.ipv4.ip_forward=1
> # increase max multicast memberships (for the Tellicast client)
> net.ipv4.igmp_max_memberships=200
> # port range for NAKs, enable only in coordination with EUMETSAT (for the Tellicast client)
> net.ipv4.ip_local_port_range=60100 61100
>```


Settable in [AWS container definitions](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-properties-ecs-taskdefinition-containerdefinition.html#cfn-ecs-taskdefinition-containerdefinition-systemcontrols), [docker compose](https://docs.docker.com/compose/compose-file/compose-file-v3/#ipv4_address-ipv6_address) or via CLI flags.

Example (docker compose):

```yaml
    sysctls:
      - net.ipv4.conf.default.rp_filter=0
      - net.ipv4.conf.all.rp_filter=0
      - net.ipv4.ip_forward=1
      - net.ipv4.igmp_max_memberships=200
      - net.ipv4.ip_local_port_range=60100 61100
```

##### Run example

```bash
#usr/bin/env bash


docker build --build-arg GIT_HASH=$(git rev-parse HEAD)   -t $(basename $(pwd)) .

docker run -e TC_USER_NAME -e TC_USER_KEY -e TC_CHANNEL_ID=1 -e TC_CHANNEL_IP=232.223.222.1 -e TC_SOURCE_IP=193.17.9.3  --rm -it --cap-add=SYS_TIME --cap-add=NET_ADMIN --device /dev/net/tun --sysctl net.ipv4.conf.default.rp_filter=0 --sysctl net.ipv4.conf.all.rp_filter=0 --sysctl net.ipv4.ip_forward=1 --sysctl net.ipv4.igmp_max_memberships=200 --sysctl net.ipv4.ip_local_port_range='60100 61100' $(basename $(pwd))
``` 

### decisions & design

The `EUMETCast Terrestrial over AMT - End-user Setup Guide.pdf` document serves as basis for the `Dockerfile` together with the `/etc/init.d` files present in the `amt` and `tellicast-client` packages.

The `Dockerfile` is a product of external packages and does not contain much custom code. It uses explicit versioning to improve reproducibility. 

The container uses a small golang program `create-tunnel` to allow non-root users to create a tunnel.

We use scripts from `vendor/*` to install non package manager dependencies to allow easier overwriting. e.g. to use your own mirrors in internet constrained environments.