module gitlab.com/KNMI-OSS/eumetcast-terrestrial-images/create-tunnel

go 1.19

require kernel.org/pub/linux/libs/security/libcap/cap v1.2.68

require kernel.org/pub/linux/libs/security/libcap/psx v1.2.68 // indirect
