// This program's purpose is to invoke ip/ifconfig passing
// capabilities to them. This allows non root users to create
// a TUN device. This program should get the following capabilities:
// CAP_NET_RAW, CAP_NET_ADMIN
//
// e.g. setcap cap_net_raw,cap_net_admin+eip ./create-tunnel

package main

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"syscall"

	"kernel.org/pub/linux/libs/security/libcap/cap"
)

func fatalOnError(e error) {
	if e != nil {
		log.Fatalf("Encountered fatal error: %s", e)
	}
}

func runCommand(args ...string) {
	cmd := exec.Command(args[0], args[1:]...)
	cmd.SysProcAttr = &syscall.SysProcAttr{
		AmbientCaps: []uintptr{
			uintptr(cap.NET_ADMIN),
			uintptr(cap.NET_RAW),
		},
	}
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	err := cmd.Run()
	fatalOnError(err)
}

func main() {

	args := os.Args[1:]

	if len(args) < 4 {
		fmt.Println("Usage: go run main.go TUNNEL TUNNEL_IP:TUNNEL_SUBNET TUNNEL_MTU PRIVATE_IP")
		return
	}

	tunnel := args[0]
	tunnelSubnet := args[1]
	tunnelMTU := args[2]
	privateIP := args[3]

	fmt.Printf("TUNNEL: %s\n", tunnel)
	fmt.Printf("TUNNEL_IP:TUNNEL_SUBNET: %s\n", tunnelSubnet)
	fmt.Printf("TUNNEL_MTU: %s\n", tunnelMTU)

	runCommand("ip", "tuntap", "add", tunnel, "mod", "tun")
	runCommand("ip", "addr", "add", tunnelSubnet, "dev", tunnel)
	runCommand("ip", "link", "set", tunnel, "up")
	runCommand("ifconfig", tunnel, "mtu", tunnelMTU, "up")
	runCommand("iptables",  "-t",  "nat", "-A",  "POSTROUTING", "--destination", "193.17.9.3/28", "-j", "SNAT", "--to-source", privateIP) 

}