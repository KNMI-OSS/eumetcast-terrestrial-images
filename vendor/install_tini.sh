#!/usr/bin/env bash
set -euo pipefail

TINI_VERSION=$1

wget -q https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini
chmod +x /tini