#!/usr/bin/env bash
set -euo pipefail

AMT_VERSION=$1

wget -q -O amt.zip "https://github.com/GrumpyOldTroll/amt/archive/${AMT_VERSION}.zip" 
unzip -q amt.zip
cd amt-${AMT_VERSION}
autoreconf -f -i                         
./configure 
make 
cp gateway/amtgwd /bin/