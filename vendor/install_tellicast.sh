#!/usr/bin/env bash
set -euo pipefail

TELLICAST_VERSION=$1

wget -q "https://sftp.eumetsat.int/public/folder/uscvknvooksycdgpmimjnq/User-Materials/EUMETCast_Support/EUMETCast_Licence_cd/Linux/Tellicast/tellicast-client-${TELLICAST_VERSION}.deb" 
dpkg -i ./tellicast-client-${TELLICAST_VERSION}.deb 
rm ./tellicast-client-${TELLICAST_VERSION}.deb 