# --- build amtgwd
ARG UBUNTU_TAG="jammy-20230916"

FROM ubuntu:$UBUNTU_TAG AS amtgwd

ENV DEBIAN_FRONTEND=noninteractive
ARG LIB_EVENT="2.1.12-stable-1build3"

RUN apt-get update  && \
    apt-get install -y automake=1:1.16.5-1.3                       \
                       build-essential=12.9ubuntu3                 \
                       ca-certificates=20230311ubuntu0.22.04.1     \
                       cmake=3.22.1-1ubuntu1.22.04.2               \
                       git=1:2.34.1-1ubuntu1.11                    \
                       libevent-dev=${LIB_EVENT}                   \
                       unzip=6.0-26ubuntu3.2                       \
                       wget=1.21.2-2ubuntu1                        \
                       --no-install-recommends &&                  \
    rm -rf /var/lib/apt/lists/*


COPY ./vendor/install_amt.sh /
ARG AMT_VERSION="be9491d17892117f139be5dd3dd055afe41bab1c"
RUN ./install_amt.sh $AMT_VERSION

# ---- build create-tunnel
FROM golang:1.19 AS builder

COPY ./create-tunnel /create-tunnel
WORKDIR /create-tunnel

ENV CGO_ENABLED=0

RUN go mod download && \
    go build 


# --- run-time image
FROM ubuntu:$UBUNTU_TAG

ENV DEBIAN_FRONTEND=noninteractive
ARG LIB_EVENT="2.1.12-stable-1build3"

RUN apt-get update &&                                                  \
    apt-get install -y                                                 \
                        ca-certificates=20230311ubuntu0.22.04.1        \
                        gettext-base=0.21-4ubuntu4                     \
                        iproute2=5.15.0-1ubuntu2                       \
                        iptables=1.8.7-1ubuntu5.2                      \
                        kmod=29-1ubuntu1                               \
                        libcap2-bin=1:2.44-1ubuntu0.22.04.1            \
                        libevent-dev=${LIB_EVENT}                      \
                        libncurses5=6.3-2ubuntu0.1                     \
                        libpcap-dev=1.10.1-4build1                     \
                        net-tools=1.60+git20181103.0eebece-1ubuntu5    \
                        wget=1.21.2-2ubuntu1                           \
                      --no-install-recommends &&                       \
    rm -rf /var/lib/apt/lists/* 

ARG TELLICAST_VERSION=2.14.7-1_amd64
COPY ./vendor/install_tellicast.sh /
RUN ./install_tellicast.sh $TELLICAST_VERSION

COPY --from=amtgwd /bin/amtgwd /bin/amtgwd

ARG TINI_VERSION=v0.19.0
COPY vendor/install_tini.sh /
RUN ./install_tini.sh $TINI_VERSION && touch /etc/tellicast-config.ini

COPY ./config/ /etc/
COPY ./run.sh /
COPY --from=builder /create-tunnel/create-tunnel /

ARG IMAGE_TAG="not set"
ENV IMAGE_TAG=$IMAGE_TAG
ARG GIT_HASH="not set"
ENV GIT_HASH=$GIT_HASH

ENV TC_PACKET_NAKS=0
# Dont run as root
RUN setcap cap_net_raw,cap_net_admin+eip /create-tunnel && \
    useradd -s /bin/bash eumetman &&                       \
    mkdir /data &&                                         \
    chmod 777 /data &&                                     \
    chown eumetman:eumetman /etc/tellicast-config.ini

USER eumetman

ENTRYPOINT ["/tini", "--"]
CMD ["./run.sh"]
